package com.dataart.opozdnyakov.poker.model.enums;

public enum CombinationEnum {
    HIGH_CARD("High card"),
    PAIR("Pair"),
    TWO_PAIR("Two pairs"),
    THREE_OF_A_KIND("Three of a kind"),
    STRAIGHT("Straight"),
    FLUSH("Flush"),
    FULL_HOUSE("Full house"),
    FOUR_OF_A_KIND("Four of a kind"),
    STRAIGHT_FLUSH("Straight flush"),
    ROYAL_FLUSH("Royal flush");

    public final String readableString;

    private CombinationEnum(String readableString) {
        this.readableString = readableString;
    }

}
