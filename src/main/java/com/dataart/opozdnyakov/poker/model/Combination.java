package com.dataart.opozdnyakov.poker.model;

import com.dataart.opozdnyakov.poker.model.enums.CombinationEnum;

import java.util.List;

public class Combination {

    private CombinationEnum combinationEnum;
    private List<Card> cards;

    public Combination(CombinationEnum combinationEnum, List<Card> cards) {
        this.combinationEnum = combinationEnum;
        this.cards = cards;
    }


    public CombinationEnum getCombinationEnum() {
        return combinationEnum;
    }

    public List<Card> getCards() {
        return cards;
    }

    public int getCardsRankSum() {
        return cards.stream()
                .map(card -> card.getRank().ordinal())
                .reduce(0, Integer::sum);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Combination that = (Combination) o;

        if (combinationEnum != that.combinationEnum) return false;
        return cards.equals(that.cards);

    }

    @Override
    public int hashCode() {
        int result = combinationEnum.hashCode();
        result = 31 * result + cards.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Combination{" +
                "combination=" + combinationEnum.name() +
                ", cards=" + cards +
                '}';
    }

}
