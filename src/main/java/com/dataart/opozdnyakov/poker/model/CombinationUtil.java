package com.dataart.opozdnyakov.poker.model;

import com.dataart.opozdnyakov.poker.model.enums.CombinationEnum;
import com.dataart.opozdnyakov.poker.model.enums.RankEnum;
import com.dataart.opozdnyakov.poker.model.enums.SuitEnum;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CombinationUtil {

    private CombinationUtil() {
    }

    public static void checkCombination(Player player, List<Card> tableCards) {
        List<Card> mergedCards = getMergedCards(player, tableCards);
        player.setCombination(findPossibleCombinations(mergedCards));
        player.setHighCard(getHighCard(mergedCards));
    }

    private static List<Card> getMergedCards(Player player, List<Card> tableCards) {
        List<Card> mergedCards = new ArrayList<>();
        mergedCards.addAll(tableCards);
        mergedCards.addAll(player.getCards());
        return mergedCards;
    }

    private static Combination findPossibleCombinations(List<Card> cards) {
        List<Combination> listOfPossibleCombinations = new ArrayList<>();
        Collections.sort(cards, Comparator.comparing(Card::getRank).reversed());
        findCombinationByRank(cards, listOfPossibleCombinations);
        findFlushCombination(cards, listOfPossibleCombinations);
        if (!listOfPossibleCombinations.isEmpty()) {
            if (listOfPossibleCombinations.size() > 1) {
                return findHighestCombinationFromPossible(listOfPossibleCombinations, cards);
            } else {
                return listOfPossibleCombinations.get(0);
            }
        } else {
            return getHighCardCombination(cards);
        }
    }

    private static void findCombinationByRank(List<Card> cards, List<Combination> possibleCombinations) {
        Map<RankEnum, List<Card>> rankMap = getRankMapFromList(cards);
        if (rankMap.keySet().size() >= 5) {
            findStraightCombination(cards, possibleCombinations);
        }
        rankMap.entrySet().forEach(entry -> {
            List<Card> combinedByRankCards = entry.getValue();
            if (combinedByRankCards.size() == 4) {
                possibleCombinations.add(new Combination(CombinationEnum.FOUR_OF_A_KIND, combinedByRankCards));
                return;
            }
            if (combinedByRankCards.size() == 3) {
                possibleCombinations.add(new Combination(CombinationEnum.THREE_OF_A_KIND, combinedByRankCards));
                return;
            }
            if (combinedByRankCards.size() == 2) {
                possibleCombinations.add(new Combination(CombinationEnum.PAIR, combinedByRankCards));
                return;
            }
        });
    }

    private static void findFlushCombination(List<Card> cards, List<Combination> possibleCombinations) {
        getSuitMapFromList(cards).entrySet().forEach(entry -> {
            List<Card> combinedBySuitCards = entry.getValue();
            if (combinedBySuitCards.size() == 5) {
                if (possibleCombinations.stream().allMatch(c -> !c.getCards().containsAll(combinedBySuitCards))) {
                    possibleCombinations.add(new Combination(CombinationEnum.FLUSH, combinedBySuitCards));
                }
            }
        });
    }

    private static void findStraightCombination(List<Card> cards, List<Combination> possibleCombination) {
        int iteration = cards.size() - 5;
        List<Card> list = IntStream.range(0, iteration + 1)
                .boxed()
                .map(i -> cards.subList(i, i + 5))
                .filter(CombinationUtil::isStraightCombination)
                .findFirst().orElse(new ArrayList<>());
        if (!list.isEmpty()) {
            addStraightCombination(list, possibleCombination);
        } else {
            if (hasAceInCards(cards)) { // ace in straight
                Collections.sort(cards, Comparator.comparing(c -> c.getRank().getAlternativeValue()));
                List<Card> cardsWithFirsAce = cards.subList(0, 5);
                if (isStraightCombinationWithFirstAce(cardsWithFirsAce)) {
                    addStraightCombination(cardsWithFirsAce, possibleCombination);
                }
            }
        }

    }

    private static void addStraightCombination(List<Card> cards, List<Combination> possibleCombination) {
        if (isSameSuitCards(cards)) {
            if (cards.get(0).getRank() == RankEnum.ACE) {
                possibleCombination.add(new Combination(CombinationEnum.ROYAL_FLUSH, cards));
            } else {
                possibleCombination.add(new Combination(CombinationEnum.STRAIGHT_FLUSH, cards));
            }
        } else {
            possibleCombination.add(new Combination(CombinationEnum.STRAIGHT, cards));
        }
    }

    private static boolean isStraightCombination(List<Card> cards) {
        boolean isStraight = IntStream.range(1, cards.size())
                .boxed()
                .allMatch(i -> cards.get(i - 1).getRank().ordinal() - cards.get(i).getRank().ordinal() == 1);
        return isStraight;
    }

    private static boolean isStraightCombinationWithFirstAce(List<Card> cards) {
        boolean isStraight = IntStream.range(1, cards.size())
                .boxed()
                .allMatch(i -> cards.get(i).getRank().getAlternativeValue() - cards.get(i - 1).getRank().getAlternativeValue() == 1);
        return isStraight;
    }

    private static boolean hasAceInCards(List<Card> cards) {
        return cards.stream().map(Card::getRank).filter(rankEnum -> rankEnum == RankEnum.ACE).findAny().isPresent();
    }

    private static Combination getHighCardCombination(List<Card> cards) {
        Card highestCard = getHighCard(cards);
        return new Combination(CombinationEnum.HIGH_CARD, Collections.singletonList(highestCard));
    }

    private static Card getHighCard(List<Card> cards) {
        return cards.stream().max(Comparator.comparing(card -> card.getRank().ordinal())).orElse(null);
    }

    private static Map<RankEnum, List<Card>> getRankMapFromList(List<Card> cards) {
        Map<RankEnum, List<Card>> map = new HashMap<>();
        Arrays.stream(RankEnum.values())
                .forEach(rank -> {
                    List<Card> cardList = cards.stream()
                            .filter(card -> card.getRank() == rank)
                            .collect(Collectors.toList());
                    if (!cardList.isEmpty()) {
                        map.put(rank, cardList);
                    }
                });
        return map;
    }

    private static Map<SuitEnum, List<Card>> getSuitMapFromList(List<Card> cards) {
        Map<SuitEnum, List<Card>> map = new HashMap<>();
        Arrays.stream(SuitEnum.values())
                .forEach(suit -> {
                    List<Card> cardList = cards.stream()
                            .filter(card -> card.getSuit() == suit)
                            .collect(Collectors.toList());
                    if (!cardList.isEmpty()) {
                        map.put(suit, cardList);
                    }
                });
        return map;
    }

    private static Combination findHighestCombinationFromPossible(List<Combination> possibleCombinations, List<Card> cards) {
        if (possibleCombinations.stream().map(Combination::getCombinationEnum)
                .collect(Collectors.toSet()).size() == 1) {
            if (hasTwoOrMorePairsCombination(possibleCombinations)) {
                return getTwoPairsCombination(possibleCombinations);
            } else {
                return getHighestFromSameCombinations(possibleCombinations);
            }
        } else {
            removeRedundantPair(possibleCombinations);
            if (hasFiveCardsInCombinations(possibleCombinations)) { // full house check
                if (hasThreeOfAKind(possibleCombinations) && hasPairCombination(possibleCombinations)) {
                    return new Combination(CombinationEnum.FULL_HOUSE, possibleCombinations.stream()
                            .flatMap(c -> c.getCards().stream())
                            .collect(Collectors.toList()));
                }
            }
            return getHighestFromDifferentCombinations(possibleCombinations);
        }
    }

    private static List<Combination> removeRedundantPair(List<Combination> possibleCombinations) {
        if (hasThreeOfAKind(possibleCombinations) && hasTwoOrMorePairsCombination(possibleCombinations)) {
            Combination minPair = possibleCombinations.stream()
                    .filter(combination -> combination.getCombinationEnum() == CombinationEnum.PAIR)
                    .min(Comparator.comparing(Combination::getCardsRankSum)).orElse(null);
            possibleCombinations.remove(minPair);
            return possibleCombinations;
        }
        return possibleCombinations;
    }

    private static boolean isSameSuitCards(List<Card> cards) {
        return cards.stream()
                .map(Card::getSuit)
                .collect(Collectors.toSet()).size() == 1;
    }

    private static Combination getHighestFromDifferentCombinations(List<Combination> combinations) {
        return combinations.stream()
                .max(Comparator.comparing(combination -> combination.getCombinationEnum().ordinal()))
                .orElse(null);
    }

    private static Combination getHighestFromSameCombinations(List<Combination> combinations) {
        return combinations.stream()
                .max(Comparator.comparing(Combination::getCardsRankSum))
                .orElse(null);
    }

    private static Combination getTwoPairsCombination(List<Combination> combinations) {
        List<Card> listOfThreePairs = combinations.stream()
                .flatMap(combination -> combination.getCards().stream())
                .sorted(Comparator.comparing(Card::getRank).reversed())
                .collect(Collectors.toList());
        return new Combination(CombinationEnum.TWO_PAIR, listOfThreePairs.subList(0, 4));
    }

    private static boolean hasPairCombination(List<Combination> combinations) {
        return combinations.stream()
                .filter(combination -> combination.getCombinationEnum() == CombinationEnum.PAIR)
                .findAny()
                .isPresent();
    }

    private static boolean hasTwoOrMorePairsCombination(List<Combination> combinations) {
        return combinations.stream()
                .filter(combination -> combination.getCombinationEnum() == CombinationEnum.PAIR)
                .count() >= 2;
    }

    private static boolean hasThreeOfAKind(List<Combination> combinations) {
        return combinations.stream()
                .filter(combination -> combination.getCombinationEnum() == CombinationEnum.THREE_OF_A_KIND)
                .findFirst()
                .isPresent();
    }

    private static boolean hasFiveCardsInCombinations(List<Combination> combinations) {
        return combinations.stream().flatMap(c -> c.getCards().stream())
                .collect(Collectors.toList()).size() == 5;
    }


}
