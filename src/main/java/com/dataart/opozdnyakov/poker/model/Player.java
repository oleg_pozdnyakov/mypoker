package com.dataart.opozdnyakov.poker.model;

import java.util.ArrayList;
import java.util.List;

public class Player {

    private String name;
    private Card highCard;
    private List<Card> cards;
    private Combination combination;
    private boolean isHuman = false;

    public Player(String name, boolean isHuman) {
        this.name = name;
        this.isHuman = isHuman;
        cards = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public Card getHighCard() {
        return highCard;
    }

    public void setHighCard(Card highCard) {
        this.highCard = highCard;
    }

    public List<Card> getCards() {
        return cards;
    }

    public Combination getCombination() {
        return combination;
    }

    public void setCombination(Combination combinationEnum) {
        this.combination = combinationEnum;
    }

    public boolean isHuman() {
        return isHuman;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", highCard=" + highCard +
                ", cards=" + cards +
                ", combination=" + combination +
                ", isHuman=" + isHuman +
                '}';
    }
}
