package com.dataart.opozdnyakov.poker.model;

import com.dataart.opozdnyakov.poker.model.enums.RankEnum;
import com.dataart.opozdnyakov.poker.model.enums.SuitEnum;

import java.util.*;

public class Deck {

    public final List<Card> cards = new ArrayList<>();
    private Random random = new Random();

    private Deck() {
    }

    public Card getCardFromDeck() {
        return cards.remove(random.nextInt(cards.size()));
    }

    public static Deck generateDeck() {
        Deck deck = new Deck();
        for (SuitEnum suitEnum : SuitEnum.values()) {
            for (RankEnum rankEnum : RankEnum.values()) {
                deck.cards.add(new Card(suitEnum, rankEnum));
            }
        }
        Collections.shuffle(deck.cards);
        return deck;
    }

    public List<Card> getCards() {
        return cards;
    }
}
