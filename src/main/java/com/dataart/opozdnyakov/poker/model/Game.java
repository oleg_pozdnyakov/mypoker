package com.dataart.opozdnyakov.poker.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Game {


    private Deck deck;
    private List<Player> players;
    private List<Card> tableCards = new ArrayList<>();

    public Game(Deck deck, List<Player> players) {
        this.deck = deck;
        this.players = players;
    }

    public void deal() {
        System.out.println("Ok. Lets deal...");
        for (Player player : players) {
            IntStream.range(0, 2)
                    .boxed()
                    .forEach(i -> player.getCards().add(deck.getCardFromDeck()));
        }
    }

    public void getFlop() {
        System.out.println("Getting flop...");
        deck.getCardFromDeck();
        IntStream.range(0, 3)
                .boxed()
                .forEach(i -> tableCards.add(deck.getCardFromDeck()));
    }

    public void getTurn() {
        System.out.println("Getting turn...");
        deck.getCardFromDeck();
        tableCards.add(deck.getCardFromDeck());
    }

    public void getRiver() {
        System.out.println("Getting river...");
        deck.getCardFromDeck();
        tableCards.add(deck.getCardFromDeck());
    }


    public List<Card> getTableCards() {
        return tableCards;
    }

    public Player getWinner(List<Player> players) {
        Player winner = null;
        int tempCombinationValue = 0;
        for (Player p : players) {
            if (tempCombinationValue != 0) {
                int playerCombinationValue = p.getCombination().getCombinationEnum().ordinal();
                if (playerCombinationValue > tempCombinationValue) {
                    winner = p;
                } else if (playerCombinationValue < tempCombinationValue) {
                    // do nothing
                } else {
                    int playerCardRankSum = p.getCombination().getCardsRankSum();
                    if (playerCardRankSum > winner.getCombination().getCardsRankSum()) {
                        winner = p;
                    }
                }
            } else {
                tempCombinationValue = p.getCombination().getCombinationEnum().ordinal();
                winner = p;
            }
        }
        return winner;
    }

}
