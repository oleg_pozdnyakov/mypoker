package com.dataart.opozdnyakov.poker.model;

import com.dataart.opozdnyakov.poker.model.enums.RankEnum;
import com.dataart.opozdnyakov.poker.model.enums.SuitEnum;

public class Card implements Comparable {

    private final SuitEnum suit;
    private final RankEnum rank;

    public Card(SuitEnum suit, RankEnum rank) {
        this.suit = suit;
        this.rank = rank;
    }

    public SuitEnum getSuit() {
        return suit;
    }

    public RankEnum getRank() {
        return rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Card card = (Card) o;

        if (suit != card.suit) return false;
        return rank == card.rank;

    }

    @Override
    public int hashCode() {
        int result = suit != null ? suit.hashCode() : 0;
        result = 31 * result + (rank != null ? rank.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "[" + suit +
                "]-[" + rank +
                ']';
    }

    @Override
    public int compareTo(Object o) throws ClassCastException {
        if (!(o instanceof Card))
            throw new ClassCastException("It is not a Card!");
        Card comparableObj = ((Card) o);
        if (rank.ordinal() > comparableObj.getRank().ordinal()) {
            return 1;
        } else if (getRank().ordinal() == comparableObj.getRank().ordinal()) {
            return 0;
        } else {
            return -1;
        }
    }
}
