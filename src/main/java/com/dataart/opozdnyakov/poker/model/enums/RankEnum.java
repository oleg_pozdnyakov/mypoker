package com.dataart.opozdnyakov.poker.model.enums;

public enum RankEnum {
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10),
    JACK(11),
    QUEEN(12),
    KING(13),
    ACE(14, 1);

    private final int value;
    private final int alternativeValue;

    RankEnum(int value) {
        this.value = value;
        this.alternativeValue = value;
    }

    RankEnum(int value, int alternativeValue) {
        this.value = value;
        this.alternativeValue = alternativeValue;
    }

    public int getValue() {
        return value;
    }

    public int getAlternativeValue() {
        return alternativeValue;
    }
}
