package com.dataart.opozdnyakov.poker.model.enums;

public enum SuitEnum {
    SPADES, HEARTS, DIAMONDS, CLUBS
}
