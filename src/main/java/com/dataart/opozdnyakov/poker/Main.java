package com.dataart.opozdnyakov.poker;

import com.dataart.opozdnyakov.poker.model.*;
import com.dataart.opozdnyakov.poker.model.enums.CombinationEnum;
import com.dataart.opozdnyakov.poker.model.enums.RankEnum;
import com.dataart.opozdnyakov.poker.model.enums.SuitEnum;
import org.apache.log4j.Logger;

import java.io.Console;
import java.util.*;
import java.util.stream.IntStream;

public class Main {

    private static Logger logger = Logger.getLogger(Main.class);

    private static final String GREETINGS = "Welcome to the poker game!";
    private static final String ENTER_NAME = "Please enter your name: ";
    private static final String PLAYERS_AGAINST = "How much players will play against you ?(max=5): ";
    private static final String WRONG_NUMBER_OF_PLAYERS = "Wrong number! by default it will be one player";
    private static final String MORE_THAN_FIVE_PLAYERS = "You can not choose more than five players, it will be five";
    private static final String CARDS_ON_THE_TABLE = "Cards on the table: ";


    public static void main(String[] args) {
        Console console = System.console();
        if (console != null) {
            Player humanPlayer = greetingAndGetHumanPlayer(console);
            List<Player> playerList = generateBots(console);
            playerList.add(humanPlayer);

            Game game = new Game(Deck.generateDeck(), playerList);

            dealCards(humanPlayer, playerList, game);

            String firstWord = console.readLine("(1) Fold (2) Check (3) Raise (4) Call: ");
            // todo need to implement logic after first word
//            System.out.println();

            openFlop(humanPlayer, playerList, game);

            String secondWord = console.readLine("(1) Fold (2) Check (3) Raise (4) Call: ");
            // todo need to implement logic

            openTurn(humanPlayer, playerList, game);
            String thirdWord = console.readLine("(1) Fold (2) Check (3) Raise (4) Call: ");
            // todo need to implement logic

            openRiver(humanPlayer, playerList, game);
            String finalWord = console.readLine("(1) Fold (2) Check (3) Raise (4) Call: ");
            // todo need to implement logic

            Player winner = game.getWinner(playerList);

            System.out.println("The winner is " + winner.getName() + " with combination "
                    + "\"" + winner.getCombination().getCombinationEnum().readableString + "\""
                    + " of " + winner.getCombination().getCards());
        } else {
            throw new RuntimeException("No console");
        }
    }

    private static Player greetingAndGetHumanPlayer(Console console) {
        logger.debug("STARTING");
        System.out.println(GREETINGS);
        String name = console.readLine(ENTER_NAME);
        System.out.printf("Hello %s! Lets start the game.\n\n", name);
        return new Player(name, true);
    }

    private static List<Player> generateBots(Console console) {
        String playersAgainst = console.readLine(PLAYERS_AGAINST);
        Integer playersAmount;
        try {
            playersAmount = Integer.parseInt(playersAgainst);
            if (playersAmount > 5) {
                System.out.println(MORE_THAN_FIVE_PLAYERS);
                playersAmount = 5;
            }
        } catch (NumberFormatException exception) {
            System.out.println(WRONG_NUMBER_OF_PLAYERS);
            playersAmount = 1;
        }
        List<Player> bots = new ArrayList<>();
        IntStream.range(0, playersAmount)
                .boxed().
                forEach(i -> bots.add(new Player("Bot" + i, false)));
        return bots;
    }

    private static void dealCards(Player humanPlayer, List<Player> playerList, Game game) {
        game.deal();
        check(playerList, game);
        printPlayerCombination(humanPlayer);
    }

    private static void openFlop(Player humanPlayer, List<Player> playerList, Game game) {
        game.getFlop();
        check(playerList, game);
        printCardOnTheTable(game);
        printPlayerCombination(humanPlayer);
    }

    private static void openTurn(Player humanPlayer, List<Player> playerList, Game game) {
        game.getTurn();
        check(playerList, game);
        printCardOnTheTable(game);
        printPlayerCombination(humanPlayer);
    }

    private static void openRiver(Player humanPlayer, List<Player> playerList, Game game) {
        game.getRiver();
        check(playerList, game);
        printCardOnTheTable(game);
        printPlayerCombination(humanPlayer);
    }

    private static void check(List<Player> players, Game game) {
        players.forEach(player -> CombinationUtil.checkCombination(player, game.getTableCards()));
    }

    private static void printPlayerCombination(Player player) {
        System.out.println("You have: " + player.getCards() + ", your combination: " + player.getCombination().getCombinationEnum().readableString + "\n");
    }

    private static void printCardOnTheTable(Game game) {
        System.out.println(CARDS_ON_THE_TABLE + game.getTableCards());
    }

}
