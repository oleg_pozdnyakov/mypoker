package com.dataart.opozdnyakov.poker.test;

import com.dataart.opozdnyakov.poker.model.Card;
import com.dataart.opozdnyakov.poker.model.Combination;
import com.dataart.opozdnyakov.poker.model.CombinationUtil;
import com.dataart.opozdnyakov.poker.model.Player;
import com.dataart.opozdnyakov.poker.model.enums.CombinationEnum;
import com.dataart.opozdnyakov.poker.model.enums.RankEnum;
import com.dataart.opozdnyakov.poker.model.enums.SuitEnum;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CombinationUtilTest {

    private static Player player = new Player("Test name", false);

    @Test
    public void shouldFindPairCombination() {
        CombinationUtil.checkCombination(player, generatePair());
        Assert.assertEquals(player.getCombination().getCombinationEnum(), CombinationEnum.PAIR);
    }

    @Test
    public void shouldFindTwoPairsCombination() {
        List<Card> cards = generatePair();
        cards.add(new Card(SuitEnum.CLUBS, RankEnum.ACE));
        cards.add(new Card(SuitEnum.SPADES, RankEnum.ACE));
        CombinationUtil.checkCombination(player, cards);
        Assert.assertEquals(player.getCombination().getCombinationEnum(), CombinationEnum.TWO_PAIR);
    }

    @Test
    public void shouldFindThreeOfAKindCombination() {
        CombinationUtil.checkCombination(player, generateThreeOfAKind());
        Assert.assertEquals(player.getCombination().getCombinationEnum(), CombinationEnum.THREE_OF_A_KIND);
    }

    @Test
    public void shouldFindFourOfAKindCombination() {
        List<Card> cards = generatePair();
        cards.add(new Card(SuitEnum.DIAMONDS, RankEnum.TWO));
        cards.add(new Card(SuitEnum.HEARTS, RankEnum.TWO));
        CombinationUtil.checkCombination(player, cards);
        Assert.assertEquals(player.getCombination().getCombinationEnum(), CombinationEnum.FOUR_OF_A_KIND);
    }

    @Test
    public void shouldFindFullHouseCombination() {
        List<Card> cards = generatePair();
        cards.addAll(generateThreeOfAKind());
        CombinationUtil.checkCombination(player, cards);
        Assert.assertEquals(player.getCombination().getCombinationEnum(), CombinationEnum.FULL_HOUSE);
    }

    @Test
    public void shouldFindFullHouseCombinationWithHighestPair() {
        List<Card> cards = generatePair();
        cards.addAll(generateThreeOfAKind());
        Card c1 = new Card(SuitEnum.DIAMONDS, RankEnum.ACE);
        Card c2 = new Card(SuitEnum.SPADES, RankEnum.ACE);
        cards.add(c1);
        cards.add(c2);
        CombinationUtil.checkCombination(player, cards);
        Assert.assertEquals(player.getCombination().getCombinationEnum(), CombinationEnum.FULL_HOUSE);
        Assert.assertTrue(player.getCombination().getCards().contains(c1));
        Assert.assertTrue(player.getCombination().getCards().contains(c2));
    }

    @Test
    public void shouldFindTwoHighestPairsFromTheePairs() {
        List<Card> cards = generatePair();
        Card cardThree1 = new Card(SuitEnum.CLUBS, RankEnum.THREE);
        Card cardThree2 = new Card(SuitEnum.HEARTS, RankEnum.THREE);
        cards.add(cardThree1);
        cards.add(cardThree2);

        Card сardAce1 = new Card(SuitEnum.CLUBS, RankEnum.ACE);
        Card сardAce2 = new Card(SuitEnum.HEARTS, RankEnum.ACE);
        cards.add(сardAce1);
        cards.add(сardAce2);
        CombinationUtil.checkCombination(player, cards);
        Assert.assertEquals(player.getCombination().getCombinationEnum(), CombinationEnum.TWO_PAIR);
        Assert.assertTrue(player.getCombination().getCards().contains(cardThree1));
        Assert.assertTrue(player.getCombination().getCards().contains(cardThree2));
        Assert.assertTrue(player.getCombination().getCards().contains(сardAce1));
        Assert.assertTrue(player.getCombination().getCards().contains(сardAce2));
        Assert.assertTrue(player.getCombination().getCards().size() == 4);
        Assert.assertTrue(!player.getCombination().getCards().containsAll(generatePair()));
    }

    @Test
    public void shouldFindFlushCombination() {
        Card c1 = new Card(SuitEnum.CLUBS, RankEnum.TWO);
        Card c2 = new Card(SuitEnum.CLUBS, RankEnum.THREE);
        Card c3 = new Card(SuitEnum.CLUBS, RankEnum.FOUR);
        Card c4 = new Card(SuitEnum.CLUBS, RankEnum.FIVE);
        Card c5 = new Card(SuitEnum.CLUBS, RankEnum.JACK);
        List<Card> cards = Arrays.asList(c1, c2, c3, c4, c5);
        CombinationUtil.checkCombination(player, cards);
        Assert.assertEquals(player.getCombination().getCombinationEnum(), CombinationEnum.FLUSH);
    }

    @Test
    public void shouldFindStraightCombination() {
        Card c1 = new Card(SuitEnum.CLUBS, RankEnum.TWO);
        Card c2 = new Card(SuitEnum.HEARTS, RankEnum.THREE);
        Card c3 = new Card(SuitEnum.DIAMONDS, RankEnum.FOUR);
        Card c4 = new Card(SuitEnum.SPADES, RankEnum.FIVE);
        Card c5 = new Card(SuitEnum.CLUBS, RankEnum.SIX);
        Card c6 = new Card(SuitEnum.CLUBS, RankEnum.JACK);
        Card c7 = new Card(SuitEnum.CLUBS, RankEnum.KING);
        List<Card> cards = Arrays.asList(c5, c2, c4, c1, c3, c6, c7);
        CombinationUtil.checkCombination(player, cards);
        Assert.assertEquals(player.getCombination().getCombinationEnum(), CombinationEnum.STRAIGHT);
    }

    @Test
    public void shouldFindStraightCombinationFromAceToFive() {
        Card c1 = new Card(SuitEnum.CLUBS, RankEnum.TWO);
        Card c2 = new Card(SuitEnum.HEARTS, RankEnum.THREE);
        Card c3 = new Card(SuitEnum.DIAMONDS, RankEnum.FOUR);
        Card c4 = new Card(SuitEnum.SPADES, RankEnum.FIVE);
        Card c5 = new Card(SuitEnum.CLUBS, RankEnum.ACE);
        Card c6 = new Card(SuitEnum.CLUBS, RankEnum.JACK);
        Card c7 = new Card(SuitEnum.CLUBS, RankEnum.KING);
        List<Card> cards = Arrays.asList(c5, c2, c4, c1, c3, c6, c7);
        CombinationUtil.checkCombination(player, cards);
        Assert.assertEquals(player.getCombination().getCombinationEnum(), CombinationEnum.STRAIGHT);
    }

    @Test
    public void shouldFindHighestStraightCombination() {
        Card c1 = new Card(SuitEnum.CLUBS, RankEnum.EIGHT);
        Card c2 = new Card(SuitEnum.HEARTS, RankEnum.NINE);
        Card c3 = new Card(SuitEnum.DIAMONDS, RankEnum.TEN);
        Card c4 = new Card(SuitEnum.SPADES, RankEnum.JACK);
        Card c5 = new Card(SuitEnum.CLUBS, RankEnum.QUEEN);
        Card c6 = new Card(SuitEnum.CLUBS, RankEnum.KING);
        Card c7 = new Card(SuitEnum.CLUBS, RankEnum.ACE);
        List<Card> cards = Arrays.asList(c5, c2, c4, c1, c3, c6, c7);
        CombinationUtil.checkCombination(player, cards);
        Assert.assertEquals(player.getCombination().getCombinationEnum(), CombinationEnum.STRAIGHT);
        Assert.assertTrue(player.getCombination().getCards().contains(c7));
        Assert.assertTrue(player.getCombination().getCards().contains(c6));
        Assert.assertTrue(player.getCombination().getCards().contains(c5));
        Assert.assertTrue(!player.getCombination().getCards().contains(c1));
        Assert.assertTrue(!player.getCombination().getCards().contains(c2));
    }


    @Test
    public void shouldFindStraightFlushCombination() {
        Card c1 = new Card(SuitEnum.CLUBS, RankEnum.TWO);
        Card c2 = new Card(SuitEnum.CLUBS, RankEnum.THREE);
        Card c3 = new Card(SuitEnum.CLUBS, RankEnum.FOUR);
        Card c4 = new Card(SuitEnum.CLUBS, RankEnum.FIVE);
        Card c5 = new Card(SuitEnum.CLUBS, RankEnum.SIX);
        List<Card> cards = Arrays.asList(c1, c2, c3, c4, c5);
        CombinationUtil.checkCombination(player, cards);
        Assert.assertEquals(player.getCombination().getCombinationEnum(), CombinationEnum.STRAIGHT_FLUSH);
    }

    @Test
    public void shouldFindRoyalFlushCombination() {
        Card c1 = new Card(SuitEnum.CLUBS, RankEnum.QUEEN);
        Card c2 = new Card(SuitEnum.CLUBS, RankEnum.JACK);
        Card c3 = new Card(SuitEnum.CLUBS, RankEnum.KING);
        Card c4 = new Card(SuitEnum.CLUBS, RankEnum.ACE);
        Card c5 = new Card(SuitEnum.CLUBS, RankEnum.TEN);
        List<Card> cards = Arrays.asList(c1, c2, c3, c4, c5);
        CombinationUtil.checkCombination(player, cards);
        Assert.assertEquals(player.getCombination().getCombinationEnum(), CombinationEnum.ROYAL_FLUSH);
    }

    @Test
    public void shouldFindHighestCombination() {
        Card c1 = new Card(SuitEnum.CLUBS, RankEnum.QUEEN);
        Card c2 = new Card(SuitEnum.HEARTS, RankEnum.QUEEN);
        Card c3 = new Card(SuitEnum.DIAMONDS, RankEnum.QUEEN);
        Card c4 = new Card(SuitEnum.SPADES, RankEnum.QUEEN);

        Card c5 = new Card(SuitEnum.CLUBS, RankEnum.THREE);
        Card c6 = new Card(SuitEnum.HEARTS, RankEnum.THREE);
        Card c7 = new Card(SuitEnum.HEARTS, RankEnum.THREE);
        List<Card> cards = Arrays.asList(c1, c2, c3, c4, c5, c6, c7);
        CombinationUtil.checkCombination(player, cards);
        Assert.assertEquals(player.getCombination().getCombinationEnum(), CombinationEnum.FOUR_OF_A_KIND);
    }

    private List<Card> generatePair() {
        List<Card> cards = new ArrayList<>();
        cards.add(new Card(SuitEnum.CLUBS, RankEnum.TWO));
        cards.add(new Card(SuitEnum.SPADES, RankEnum.TWO));
        return cards;
    }

    private List<Card> generateThreeOfAKind() {
        List<Card> cards = new ArrayList<>();
        cards.add(new Card(SuitEnum.CLUBS, RankEnum.THREE));
        cards.add(new Card(SuitEnum.SPADES, RankEnum.THREE));
        cards.add(new Card(SuitEnum.HEARTS, RankEnum.THREE));
        return cards;
    }

}
